package com.cnglobal.expeedio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class TestBase {
	public static Properties CONFIG=null;
	public static Properties OR=null;
	public static boolean isLoggedOn=false;
	public static WebDriver dr=null;
	public static EventFiringWebDriver driver=null;
	
	
	public void initialize() throws IOException{
		
		if(driver==null){
			
/*			System.setProperty("webdriver.gecko.driver", "c:\\Automation\\software\\Drivers\\geckodriver.exe");
			WebDriver driver = new FirefoxDriver();
			driver.get("https://google.com");*/
			
			//----- Initialize CONFIG
			CONFIG=new Properties();
			FileInputStream fileinputstream=new FileInputStream(System.getProperty("user.dir")+"//src//test//resources//config//config.properties");
			CONFIG.load(fileinputstream);
			//----- Initialize OR
			OR=new Properties();
			fileinputstream=new FileInputStream(System.getProperty("user.dir")+"//src//test//resources//config//OR.properties");
			OR.load(fileinputstream);
			
			if(CONFIG.getProperty("browser").equals("Firefox")){
				System.setProperty("webdriver.gecko.driver", "c:\\Automation\\software\\Drivers\\geckodriver.exe");
				dr=new FirefoxDriver();
			}else if(CONFIG.getProperty("browser").equals("Chrome")){
				System.setProperty("webdriver.chrome.driver", "c:\\Automation\\software\\Drivers\\chromedriver.exe");
				dr=new ChromeDriver();
			}else if(CONFIG.getProperty("browser").equals("IE")){
				dr=new InternetExplorerDriver();
			}
			
			driver=new EventFiringWebDriver(dr);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
		

		
	}
	
	
	public static WebElement getObject(String xpathexp){
		return driver.findElement(By.xpath(OR.getProperty(xpathexp)));
		
	}
	
}
